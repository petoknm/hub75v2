`timescale 1ns/100ps

module test_bam;

  reg clk = 0;
  reg next = 1;
  wire [2:0] bit;

  bam b (clk, next, bit);

  initial begin
     $dumpvars(0, test_bam);

     # 1000 $finish;
  end

  always #1 clk = !clk;

endmodule // test
