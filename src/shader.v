module shader(clk, x, y, start, rgb, ready);

input start, clk;
input [5:0] x, y;
output [23:0] rgb;
output ready;

reg [23:0] rgb;
reg ready = 0;

always @(posedge clk) begin

  ready <= 0;

  if (start) begin
    rgb <= 24'h0000FF;
    ready <= 1;
  end

end

endmodule // shader
