`timescale 1ns/100ps

module test_main;

  reg clk = 0;
  reg reset = 1;

  main m (clk, reset);

  initial begin
     $dumpvars(0, test_main);

     # 4 reset = 0;
     # 1000 $finish;
  end

  always #1 clk = !clk;

  // initial
  //    $monitor("At time %t, value = %h (%0d)",
  //             $time, value, value);
endmodule // test_main
