`timescale 1ns/100ps

module test_line_modulator;

  reg clk = 0;
  reg we = 1;
  reg [5:0] waddr = 0;
  reg [23:0] wdata = 24'hAA0055;
  reg swap = 0;
  wire [5:0] raddr;
  wire [23:0] rdata;

  wire led_clk, led_oe, led_lat;
  wire [2:0] led_rgb;

  line_buffer    lb(clk, we, waddr, wdata, swap, raddr, rdata);
  line_modulator lm(clk, raddr, rdata, led_clk, led_oe, led_lat, led_rgb);

  initial begin
     $dumpvars(0, test_line_modulator);

     # 2 swap = 1;
     # 2 swap = 0;
     # 10000 $finish;
  end

  always #1 clk = !clk;

endmodule // test
