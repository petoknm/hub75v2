`timescale 1ns/100ps

module test_line_filler;

  reg [5:0] y = 0;
  reg begin_line = 0;
  reg clk = 0;
  wire we;
  wire [5:0] waddr;
  wire [23:0] wdata;
  wire swap;
  reg [5:0] raddr = 5'h10;
  wire [23:0] rdata;

  line_filler lf(clk, y, begin_line, we, waddr, wdata, swap);
  line_buffer lb(clk, we, waddr, wdata, swap, raddr, rdata);

  initial begin
     $dumpvars(0, test_line_filler);

     # 2;
     begin_line = 1;
     # 2;
     begin_line = 0;
     # 200;
     begin_line = 1;
     # 2;
     begin_line = 0;
     # 1000;
     $finish;
  end

  always #1 clk = !clk;

endmodule // test
