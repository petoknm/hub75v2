module line_modulator(clk, lb_raddr, lb_rdata, led_clk, led_oe, led_lat, led_rgb);

localparam  PUSHING = 3'd1,
            OE = 3'd2,
            OE_LAT = 3'd3;

input clk;
input [23:0] lb_rdata;
output [5:0] lb_raddr;
output led_clk, led_oe, led_lat;
output [2:0] led_rgb;

reg [7:0] counter = 0;
wire [2:0] bit;
wire [2:0] led_rgb;

bam b(clk, counter_reset, bit);
bit_slicer s(lb_rdata, bit, led_rgb);

reg [2:0] state = PUSHING;
wire counter_reset = counter == 8'h10;
reg [5:0] lb_raddr = 0;

always @(posedge clk) begin
  led_lat <= 0;
  
  case(state)
    PUSHING: begin

    end

    OE: led_oe <= 0;
    OE_LAT: begin
      led_oe <= 0;
      led_lat <= 1;
    end
  endcase
end

endmodule // line_modulator
