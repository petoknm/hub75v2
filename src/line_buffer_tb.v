`timescale 1ns/100ps

module test_line_buffer;

  reg clk = 0;
  reg we = 1;
  reg [5:0] waddr = 0;
  reg [23:0] wdata = 24'hFF00FF;
  reg swap = 0;
  reg [5:0] raddr = 0;
  wire [23:0] rdata;

  line_buffer lb (clk, we, waddr, wdata, swap, raddr, rdata);

  initial begin
     $dumpvars(0, test_line_buffer);

     # 2 swap = 1;
     # 2 swap = 0;
     # 1000 $finish;
  end

  always #1 clk = !clk;

endmodule // test
