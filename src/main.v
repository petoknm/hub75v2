module main(clk, reset);

  input clk, reset;

  reg led_clk, led_oe, led_lat;
  reg [5:0] led_rgb = 0;
  reg [4:0] led_addr = 0;

  always @(posedge clk)
    if (reset)
      led_addr <= 0;
    else
      led_addr <= led_addr + 1;

endmodule // main
