module line_filler(clk, y, begin_line, lb_we, lb_waddr, lb_wdata, lb_swap);

localparam  IDLE = 3'd1,
            START_SHADER = 3'd2,
            WAIT_FOR_SHADER = 3'd4;

input clk, begin_line;
input [5:0] y;
output lb_we, lb_swap;
output [5:0] lb_waddr;
output [23:0] lb_wdata;

wire [23:0] rgb;
wire ready;
wire start = state == START_SHADER;
reg lb_we = 0;
reg lb_swap = 0;
reg [5:0] lb_waddr = 0;
wire [23:0] lb_wdata = rgb;

reg [5:0] x = 0;
reg [2:0] state = IDLE;

shader s(clk, x, y, start, rgb, ready);

always @(posedge clk) begin
  lb_swap <= 0;
  lb_we <= 0;

  case(state)
    IDLE:             if (begin_line) state <= START_SHADER;
    START_SHADER:     state <= WAIT_FOR_SHADER;
    WAIT_FOR_SHADER:  if (ready) begin
      x <= x + 1;
      lb_waddr <= x;
      lb_we <= 1;
      if (x == 6'h1F) begin
        lb_swap <= 1;
        state <= IDLE;
      end
      else state <= START_SHADER;
    end
  endcase
end

endmodule // line_filler
