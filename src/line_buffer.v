module line_buffer(clk, we, waddr, wdata, swap, raddr, rdata);

input clk, we, swap;
input [5:0] waddr, raddr;
input [23:0] wdata;
output [23:0] rdata;

reg [23:0] rdata = 0;
reg bank_sel = 0;
wire [6:0] buf_waddr = { bank_sel, waddr };
wire [6:0] buf_raddr = { !bank_sel, raddr };

reg [23:0] buf_ [6:0];

always @(posedge clk) begin
  if (swap)
    bank_sel <= !bank_sel;

  if (we)
    buf_[buf_waddr] <= wdata;

  rdata <= buf_[buf_raddr];
end

endmodule // line_buffer
