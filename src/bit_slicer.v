module bit_slicer(rgb_in, bit, rgb_out);

input [23:0] rgb_in;
input [2:0] bit;
output [2:0] rgb_out;

wire [7:0] r = rgb_in[ 7: 0];
wire [7:0] g = rgb_in[15: 8];
wire [7:0] b = rgb_in[23:16];
wire [2:0] rgb_out = { b[bit], g[bit], r[bit] };

endmodule // bit_slicer
