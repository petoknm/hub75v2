module bam(clk, next, bit);

input clk, next;
output [2:0] bit;

reg [2:0] bit = 0;
reg [7:0] counter = 0;

wire [7:0] counter_next = counter + 1;

always @(posedge clk) begin
  if (next) begin
    counter <= counter_next;

    if (counter_next == (1 << bit)) begin
      counter <= 0;
      bit <= bit + 1;
    end
  end
end

endmodule // bam
