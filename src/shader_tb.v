`timescale 1ns/100ps

module test_shader;

  reg clk = 0;
  reg start = 0;
  reg [5:0] x = 0;
  reg [5:0] y = 0;
  wire [23:0] rgb;
  wire ready;

  shader s (clk, x, y, start, rgb, ready);

  initial begin
     $dumpvars(0, test_shader);

     # 10 start = 1;
     # 2 start = 0;
     # 10 start = 1;
     # 2 start = 0;
     # 1000 $finish;
  end

  always #1 clk = !clk;

endmodule // test
