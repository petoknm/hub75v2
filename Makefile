SRC := $(filter-out %_tb.v, $(wildcard src/*.v))
TB  := $(filter-out test, $(MAKECMDGOALS))

.PHONY: test

test: gtkwave

gtkwave: dump.vcd
	gtkwave $<

dsn.vvp: $(SRC) $(TB)
	iverilog -o $@ $^

dump.vcd: dsn.vvp
	vvp $<

clean:
	rm dsn.vvp dump.vcd
